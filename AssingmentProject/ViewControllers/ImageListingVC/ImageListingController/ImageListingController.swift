//
//  ImageListingController.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
protocol ImageListingControllerDelegate: class {
    func showErrorMessage(message: String)
    func didFetchedFlickerImages()
}

class ImageListingController  {
    
    //MARK:- Public Properties
    //========================
    weak var delegate: ImageListingControllerDelegate?
    var isSearching: Bool = false
    var viewId: String = ""
    var flickrImageList: [FlickrImageModel] {
        return isSearching ? searchedFlickrImageList : normalFlickrImageList
    }
    
    var showPaginationLoader: Bool {
        get {
            return (isSearching ? searchedPage : normalPage) > 0
        }
    }
    
    
    //MARK:- Private Properties
    //=========================
    private var normalFlickrImageList: [FlickrImageModel] = [FlickrImageModel]()
    private var searchedFlickrImageList: [FlickrImageModel] = [FlickrImageModel]()
    private var normalPage = 1
    private var normalTotalCount = 1
    private var normalIsRequestingApi = false
    private var searchedPage = 1
    private var searchedTotalCount = 1
    private var searchedIsRequestingApi = false
    private var searchText = ""
    
    //MARK:- Methods
    //==============
    func fetchFlickrImageList() {
        hitFetchContactListApi(isSearching: self.isSearching)
    }
    
    func searchFlickrImageForText(searchText: String) {
        
        self.searchText = searchText
        if !self.searchText.isEmpty {
            self.searchedPage = 1
            self.searchedIsRequestingApi = false
            self.isSearching = true
            hitFetchContactListApi(isSearching: isSearching)
        } else {
            self.isSearching = false
        }
        self.searchedFlickrImageList.removeAll()
        self.delegate?.didFetchedFlickerImages()
    }
    
    func pullToRefreshData() {
        
        if isSearching {
            self.searchedPage = 1
            self.searchedIsRequestingApi = false
        } else {
            self.normalPage = 1
            self.normalIsRequestingApi = false
        }
        hitFetchContactListApi(isSearching: isSearching)
    }
    
    
    //MARK:- Api Call Methods
    //=========================
    private func hitFetchContactListApi(isSearching: Bool) {
        
        guard (isSearching ? searchedPage : normalPage) > 0,
            !(isSearching ? searchedIsRequestingApi : normalIsRequestingApi) else {return}
        
        if isSearching {
            self.searchedIsRequestingApi = true
        }
        else {
            self.normalIsRequestingApi = true
        }
        
        
        let parameters: JSONDictionary = [
            ApiKey.method: (isSearching ? "flickr.photos.search" : "flickr.photos.getRecent"),
            ApiKey.noJsonCallBack: 1,
            ApiKey.page: (isSearching ? searchedPage : normalPage),
            ApiKey.format: "json",
            ApiKey.searchText: (isSearching ? self.searchText : ""),
            ApiKey.extras: ApiKey.imgUrl,
            ApiKey.apiKey: AppConstants.flickrApiKey.rawValue,
            ]
        
        WebServices.getImages(parameters: parameters, success: { [weak self] (flickerImageList, nextPageRecord, totalRecords) in
            
            guard let strongSelf = self else { return }
            
            if parameters[ApiKey.page] as? Int == 1 {
                if isSearching {
                    strongSelf.searchedFlickrImageList.removeAll()
                } else {
                    strongSelf.normalFlickrImageList.removeAll()
                }
            }
            if isSearching {
               strongSelf.searchedPage = !flickerImageList.isEmpty ? strongSelf.searchedPage + 1 : 0
                strongSelf.searchedIsRequestingApi = false
                strongSelf.searchedTotalCount = totalRecords
                strongSelf.searchedFlickrImageList.append(contentsOf: flickerImageList)
            } else {
                strongSelf.normalPage = !flickerImageList.isEmpty ? strongSelf.normalPage + 1 : 0
                strongSelf.normalIsRequestingApi = false
                strongSelf.normalTotalCount = totalRecords
                strongSelf.normalFlickrImageList.append(contentsOf: flickerImageList)
            }
            strongSelf.delegate?.didFetchedFlickerImages()
            
        }) { [weak self] (error) -> (Void) in
            
            guard let strongSelf = self else { return }
            
           
                if isSearching {
                    strongSelf.searchedPage = 0
                    strongSelf.searchedIsRequestingApi = false
                    //                    strongSelf.searchedTotalCount = 0
                    //                    strongSelf.searchedClientList.removeAll()
                } else {
                    strongSelf.normalPage = 0
                    strongSelf.normalIsRequestingApi = false
                    //                    strongSelf.normalTotalCount = 0
                    //                    strongSelf.normalClientList.removeAll()
                }
            
            
            strongSelf.delegate?.didFetchedFlickerImages()
            print_debug(error)
        }
        
        
    }
}
