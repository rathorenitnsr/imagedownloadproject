//
//  ImageListingVC.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ImageListingVC: BaseVC {
    
    enum NumberOfItemsInRow: Int{
        case two = 2
        case three
        case four
        
    }
    
    // MARK: IBOutlets
    //================
    @IBOutlet weak var imageListingColletionView: UICollectionView!
    
    // MARK: Properties
    //=================
    var itemsPerRow = NumberOfItemsInRow.two {
        didSet {
            imageListingColletionView.performBatchUpdates({
                imageListingColletionView.collectionViewLayout.invalidateLayout()
            }, completion: nil)
            
        }
    }
    var sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    var selectedIndexPath: IndexPath!
    var viewController = ImageListingController()
    
    //MARK:- Private Properties
    //==============================
    private let refreshController = UIRefreshControl()
    private var searchBar = UISearchBar()
    
    
    //MARK:- View Life Cycle
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        setUpForRefreshController()
        setupNavBar()
        
    }
    
    override func bindController() {
        viewController.delegate = self
    }
    
    //MARK:- @IBAction
    //================
    @objc func pullToRefreshData() {
        
        self.viewController.pullToRefreshData()
    }
    
    @objc  func rightNavBarBtnTapped(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        showAlertToSelectItemSize()
    }
}

// MARK: Extension for private methods
//=====================================
extension ImageListingVC {
    
    private func initialSetup() {
        registerXibs()
        imageListingColletionView.dataSource = self
        imageListingColletionView.delegate = self
    }
    
    private func registerXibs() {
        imageListingColletionView.registerCell(with: ImageCollectionViewCell.self)
        imageListingColletionView.registerFooterView(with: ImageCollectionViewFooterView.self)
    }
    
    private func setUpForRefreshController() {
        
        refreshController.tintColor = UIColor.gray.withAlphaComponent(0.6)
        refreshController.addTarget(self,
                                    action: #selector(pullToRefreshData),
                                    for: UIControl.Event.valueChanged)
        if #available(iOS 10.0, *) {
            imageListingColletionView.refreshControl = refreshController
        } else {
            imageListingColletionView.addSubview(refreshController)
        }
    }
    
    private func setupNavBar() {
        
        searchBar.delegate = self
        searchBar.placeholder = "Search Images..."
        
        navigationItem.titleView = searchBar
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icMore"), style: .plain, target: self, action: #selector(rightNavBarBtnTapped))
    }
    
    func moveToImageDetail() {
        
        let imageDetailScene = ImageDetailVC.instantiate(fromAppStoryboard: .main)
        imageDetailScene.imageModel = viewController.flickrImageList[selectedIndexPath.item]
        imageDetailScene.transitionController.fromDelegate = self
        imageDetailScene.transitionController.toDelegate = imageDetailScene
        self.navigationController?.delegate = imageDetailScene.transitionController
        self.navigationController?.pushViewController(imageDetailScene, animated: true)
    }
    
    
    
    private func showAlertToSelectItemSize() {
        
        let actionSheetAlertController = UIAlertController(title: nil, message: "Options", preferredStyle: .actionSheet)
        
        let twoItemsPerRowAction = UIAlertAction(title: "Two Items", style: .default) { _ in
            self.itemsPerRow = .two
        }
        
        let threeItemsPerRowAction = UIAlertAction(title: "Three Item", style: .default) { _ in
            self.itemsPerRow = .three
        }
        
        let fourItemsPerRowAction = UIAlertAction(title: "Four Item", style: .default) { _ in
            self.itemsPerRow = .four
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        switch self.itemsPerRow  {
        case .two:
            actionSheetAlertController.addAction(threeItemsPerRowAction)
            actionSheetAlertController.addAction(fourItemsPerRowAction)
        case .three:
            actionSheetAlertController.addAction(twoItemsPerRowAction)
            actionSheetAlertController.addAction(fourItemsPerRowAction)
        case .four:
            actionSheetAlertController.addAction(twoItemsPerRowAction)
            actionSheetAlertController.addAction(threeItemsPerRowAction)
        }
        
        actionSheetAlertController.addAction(cancelAction)
        
        present(actionSheetAlertController, animated: true, completion: nil)
    }
}

//MARK:- Extension for UISearchBar Delegate
//=========================================
extension ImageListingVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        guard var searchText = searchBar.text else { return }
        searchText = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
        self.viewController.searchFlickrImageForText(searchText: searchText)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print_debug(searchText)
        if searchText.isEmpty {
            self.viewController.searchFlickrImageForText(searchText: searchText)
        }
    }
    
}

// MARK: Extension for ImageListingController Delegate
//====================================================
extension ImageListingVC: ImageListingControllerDelegate {
    func showErrorMessage(message: String) {
        print_debug(message)
    }
    
    func didFetchedFlickerImages() {
        DispatchQueue.main.async {
            self.imageListingColletionView.reloadData()
        }
    }
    
}

// MARK: Extension for ZoomAnimator Delegate
//==========================================
extension ImageListingVC: ZoomAnimatorDelegate {
    
    func transitionWillStartWith(zoomAnimator: ZoomAnimator) {
        
    }
    
    func transitionDidEndWith(zoomAnimator: ZoomAnimator) {
        
    }
    
    func referenceImageView(for photosAnimator: ZoomAnimator) -> UIImageView? {
        if let cell = imageListingColletionView.cellForItem(at: selectedIndexPath) as? ImageCollectionViewCell {
            return cell.imageView
        }
        return nil
    }
    
    func referenceImageViewFrameInTransitioningView(for photosAnimator: ZoomAnimator) -> CGRect? {
        
        guard let cell = imageListingColletionView.cellForItem(at: selectedIndexPath) as? ImageCollectionViewCell else {
            return nil
        }
        
        let cellFrame = imageListingColletionView.convert(cell.frame, to: view)
        
        if cellFrame.minY < imageListingColletionView.contentInset.top {
            return CGRect(x: cellFrame.minX, y: imageListingColletionView.contentInset.top, width: cellFrame.width, height: cellFrame.height - (imageListingColletionView.contentInset.top - cellFrame.minY))
        }
        
        return cellFrame
    }
}
