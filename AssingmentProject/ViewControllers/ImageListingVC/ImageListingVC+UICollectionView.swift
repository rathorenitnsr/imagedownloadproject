//
//  ImageListingVC+UICollectionView.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

// MARK: Collection View DataSource Methods
//=========================================
extension ImageListingVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewController.flickrImageList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueCell(with: ImageCollectionViewCell.self, indexPath: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let reusableView = collectionView.dequeueFooterView(with: ImageCollectionViewFooterView.self, indexPath: indexPath)
        
        return reusableView
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        // Hitting api for pagination
        if view.isKind(of: ImageCollectionViewFooterView.self) {
            viewController.fetchFlickrImageList()
        }
    }
}

// MARK: Collection View Delegate Methods
//=======================================
extension ImageListingVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        moveToImageDetail()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let image = viewController.flickrImageList[indexPath.row]
        guard !image.isDownloadingInProgress else  { return }
        
        ImageDownloader.shared.downloadImage(at: indexPath, model: image, completion: { [weak self] (indexPath, image, error) in
            
            DispatchQueue.main.async { [weak self] in
                
                guard let strongSelf = self else {
                    return
                }
                
                guard let idxPath = indexPath,
                    let cell = strongSelf.imageListingColletionView.cellForItem(at: idxPath) as? ImageCollectionViewCell else {return}
                
                if (error == nil) {
                    cell.imageView.image = image
                    cell.imageView.contentMode = .scaleAspectFill
                    
                } else {
                    cell.imageView.image = #imageLiteral(resourceName: "PlaceHolderImage")
                    cell.imageView.contentMode = .center
                }
                
            }
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewController.flickrImageList.indices.contains(indexPath.row) { viewController.flickrImageList[indexPath.row].downloadingTask?.priority = URLSessionDataTask.lowPriority
        }
    }
    
    
}

// MARK: Collection View FlowLayout Delegate Methods
//===================================================
extension ImageListingVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let minimumInterItemSpacing: CGFloat = 10
        let side = (collectionView.frame.width - sectionInset.left - sectionInset.right - CGFloat(itemsPerRow.rawValue - 1)*minimumInterItemSpacing) / CGFloat(itemsPerRow.rawValue)
        return CGSize(width: side, height: side)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInset
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        return viewController.showPaginationLoader ? CGSize(width: collectionView.frame.width, height: 60) : CGSize.zero
    }
}
