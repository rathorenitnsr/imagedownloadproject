//
//  ImageDetailVC.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ImageDetailVC: BaseVC {

    // MARK: IBOutlets
    @IBOutlet weak var imageDetailView: UIImageView!
    
    // MARK: Public Properties
    var imageModel: FlickrImageModel?
    var transitionController = ZoomTransitionController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
    }
        

}

// MARK: Extension for private methods
//=====================================
extension ImageDetailVC {
    
    private func initialSetup() {
        imageDetailView.image = #imageLiteral(resourceName: "ic_placeholder")
        imageDetailView.contentMode = .scaleAspectFit
        
        guard let object = imageModel else {return}
        ImageDownloader.shared.downloadImage(model: object) { [weak self] (_, image, error) in
            DispatchQueue.main.async { [weak self] in
                
                guard let strongSelf = self else {
                    return
                }
                
                if let err = error {
                    print_debug(err.localizedDescription)
                } else {
                    strongSelf.imageDetailView.image = image
                }
            }
        }
    }
}

// MARK: Extension for private methods
//=====================================
extension ImageDetailVC: ZoomAnimatorDelegate {
    func transitionWillStartWith(zoomAnimator: ZoomAnimator) {
        
    }
    
    func transitionDidEndWith(zoomAnimator: ZoomAnimator) {
        
    }
    
    func referenceImageView(for photosAnimator: ZoomAnimator) -> UIImageView? {
        return imageDetailView
    }
    
    func referenceImageViewFrameInTransitioningView(for photosAnimator: ZoomAnimator) -> CGRect? {
        return imageDetailView.frame
    }
 
}
