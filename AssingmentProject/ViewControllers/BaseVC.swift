//
//  BaseVC.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        bindController()
    }

}

//MARK:- Extension for Methods
//============================
extension BaseVC {
    
    @objc func bindController() {
        
    }
}

