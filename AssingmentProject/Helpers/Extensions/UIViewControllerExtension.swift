//
//  UIViewControllerExtension.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 15/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // Not using static as it won't be possible to override to provide custom storyboardID then
    class var storyboardID : String {
        return "\(self)"
    }
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(self)
    }
    
}
