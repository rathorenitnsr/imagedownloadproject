//
//  Utilities.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

func print_debug <T> (_ object: T) {
    
    // TODO: Comment Next Statement To Deactivate Logs
    if isSimulatorDevice {
        print(object)
    } else {
        print(object)
    }
    
}

var isSimulatorDevice:Bool {
    
    var isSimulator = false
    #if arch(i386) || arch(x86_64)
    //simulator
    isSimulator = true
    #endif
    return isSimulator
}
