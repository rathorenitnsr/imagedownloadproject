//
//  AppStoryboards.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 15/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

enum AppStoryboard : String {
    case main
    
    var name: String {
        return self.rawValue.capitalized
    }
    
}

extension AppStoryboard {
    
    var instance : UIStoryboard {
        
        return UIStoryboard(name: self.name, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(_ viewControllerClass : T.Type,
                                              function : String = #function, // debugging purposes
        line : Int = #line,
        file : String = #file) -> T {
        
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.name) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
        }
        
        return scene
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}
