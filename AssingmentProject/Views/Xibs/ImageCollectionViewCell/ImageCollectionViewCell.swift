//
//  ImageCollectionViewCell.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
   
    // MARK: IBOutlets
    // ================
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageContainerView: UIView!
    
    // MARK: CollectionView Cell Life Cycle Methods
    // =============================================
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialSetUp()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        initialSetUp()
    }
    
    // MARK: Private Methods
    
    // Method Fot initial setup
    private func initialSetUp() {
        imageView.image = #imageLiteral(resourceName: "ic_placeholder")
        imageView.contentMode = .scaleAspectFit
    }
}

