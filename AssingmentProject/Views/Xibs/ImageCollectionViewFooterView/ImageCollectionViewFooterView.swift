//
//  ImageCollectionViewFooterView.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ImageCollectionViewFooterView: UICollectionReusableView {
    
    // MARK: IBOutlets
    // ================
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialSetUp()
    }
    
    // MARK: Private Methods
    
    // Method Fot initial setup
    private func initialSetUp() {
        
    }
}
