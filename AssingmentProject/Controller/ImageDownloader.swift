//
//  ImageDownloader.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
// refrence https://stackoverflow.com/questions/24231680/loading-downloading-image-from-url-on-swift
class ImageDownloader {
    
    static let shared = ImageDownloader()
    
    private let imageCache = NSCache<NSString, UIImage>()
    private let session = URLSession.shared
    
    func downloadImage(at indexPath: IndexPath? = nil, model: FlickrImageModel, completion: @escaping (IndexPath?, UIImage?, Error?) -> Void) {
        
        if let cachedImage = imageCache.object(forKey: model.imgUrl.absoluteString as NSString) {
            completion(indexPath, cachedImage, nil)
            
        } else {
            downloadDataForImage(model, completion: { (image, error) in
                completion(indexPath, image, error)
            })
        }
    }
    
    
    private func downloadDataForImage(_ model: FlickrImageModel, completion: @escaping ((UIImage, Error?) -> ())) {
        
        model.isDownloadingInProgress = true
        
        let downloadTask = self.session.dataTask(with: model.imgUrl, completionHandler: { (data, response, error) in
            
            model.isDownloadingInProgress = false
            model.downloadingTask = nil
            
            if let error = error {
                print_debug(error)
                return
            }
            
            if data != nil {
                
                if let downloadedImage = UIImage(data: data!) {
                    self.imageCache.setObject(downloadedImage, forKey: model.imgUrl.absoluteString as NSString)
                    completion(downloadedImage, nil)
                    
                }
            }
        })
        
        downloadTask.priority = URLSessionDataTask.lowPriority
        model.downloadingTask = downloadTask
        downloadTask.resume()
    }
}
