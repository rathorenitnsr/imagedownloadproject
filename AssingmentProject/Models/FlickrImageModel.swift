//
//  FlickrImageModel.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import SwiftyJSON

class FlickrImageModel {
    
    let imgUrl: URL
    var isDownloadingInProgress: Bool
    var downloadingTask: URLSessionDataTask?
    
    init?(with data: JSON) {
        
        guard let url = URL(string: data[ApiKey.imgUrl].stringValue) else {
                return nil
        }
        
        imgUrl = url
        isDownloadingInProgress = false
        
    }
    
    static func getModels(from data: JSON) -> [FlickrImageModel] {
        
        
        let images: [FlickrImageModel] = data.arrayValue.map { dictionary -> FlickrImageModel? in
            
            if let image = FlickrImageModel(with: dictionary) {
                return image
            }
            return nil
            
            }.filter { (image) -> Bool in
                return (image != nil)
            }.map { (image) -> FlickrImageModel in
                return image!
        }
        return images
    }
}

/*
 {
 farm = 66;
 "height_m" = 331;
 id = 47850927271;
 isfamily = 0;
 isfriend = 0;
 ispublic = 1;
 owner = "26132493@N02";
 secret = 644c28c5ea;
 server = 65535;
 title = tyre;
 "url_m" = "https://live.staticflickr.com/65535/47850927271_644c28c5ea.jpg";
 "width_m" = 500;
 }
 */
