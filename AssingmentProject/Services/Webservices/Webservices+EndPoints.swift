//
//  Webservices+EndPoints.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

let BASE_URL = "https://api.flickr.com/"

extension WebServices {
    
    enum EndPoint : String {
        
        case services = "services/rest"
        
        var path : String {
            return BASE_URL + self.rawValue
        }
    }
}
