//
//  Webservices+Key.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

enum ApiKey {
    
    
    static var apiKey: String { return "api_key" }
    static var method: String { return "method" }
    static var searchText: String { return "text" }
    static var extras: String { return "extras" }
    static var format: String { return "format" }
    static var noJsonCallBack: String { return "nojsoncallback" }
    static var photo: String { return "photo" }
    static var photos: String { return "photos" }
    static var page: String { return "page" }
    static var pages: String { return "pages" }
    static var perPage: String { return "perpage" }
    static var total: String { return "total" }
    static var imgUrl: String { return "url_m" }

}

