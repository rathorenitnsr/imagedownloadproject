//
//  Webservices.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire


enum WebServices { }

typealias FlickrImageApiResponse = (([FlickrImageModel], Int, Int) -> Void)

extension NSError {
    
    convenience init(localizedDescription : String) {
        
        self.init(domain: "AppNetworkingError",
                  code: 0,
                  userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
    
    convenience init(code : Int, localizedDescription : String) {
        
        self.init(domain: "AppNetworkingError",
                  code: code,
                  userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
    
}



extension WebServices {
    
    
    //MARK:- Get Image
    //===============
    static func getImages(parameters : JSONDictionary,
                          loader : Bool = true,
                          success : @escaping FlickrImageApiResponse,
                          failure : @escaping (Error) -> Void) {
        
        print_debug("")
        print_debug("=================Get Images===================")
        print_debug("Server URL")
        print_debug(WebServices.EndPoint.services.path)
        print_debug(parameters)
        print_debug("============================================")
        print_debug("")
        
        AppNetworking.GET(endPoint: WebServices.EndPoint.services, parameters: parameters, loader: loader, success: { (json : JSON) in
            let photos = json[ApiKey.photos]
            let currentPage = photos[ApiKey.page].intValue
            let noOfPages = photos[ApiKey.pages].intValue
            let images = FlickrImageModel.getModels(from: photos[ApiKey.photo])
            success(images, currentPage, noOfPages)
            
        }) { (error) in
            
            failure(error)
            print_debug(error)
        }
    }
}
