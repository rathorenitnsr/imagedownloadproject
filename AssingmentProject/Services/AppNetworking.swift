//
//  AppNetworking.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

typealias JSONDictionary = [String: Any]
typealias JSONDictionaryArray = [JSONDictionary]
typealias SuccessResponse = (_ json: JSON) -> ()
typealias FailureResponse = (Error) -> (Void)
//typealias UserControllerSuccess = (_ user: User) -> ()


extension Notification.Name {
    
    static let NotConnectedToInternet = Notification.Name("NotConnectedToInternet")
   
}

enum AppNetworking {
    
    
    static let username = "admin"
    static let password = "12345"
    fileprivate static var alamofireManager: SessionManager!
    
    static func configureAlamofire() {
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30 // seconds
        configuration.timeoutIntervalForResource = 30
        self.alamofireManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    static func POST(endPoint : WebServices.EndPoint,
                     parameters : JSONDictionary = [:],
                     headers : HTTPHeaders = [:],
                     loader : Bool = true,
                     success : @escaping (JSON) -> Void,
                     failure : @escaping (Error) -> Void) {
        
        
        request(URLString: endPoint.path,
                httpMethod: .post,
                parameters: parameters,
                headers: headers,
                loader: loader,
                success: success,
                failure: failure)
    }
    
    
    
    
    static func GET(endPoint : WebServices.EndPoint,
                    parameters : JSONDictionary = [:],
                    headers : HTTPHeaders = [:],
                    loader : Bool = true,
                    success : @escaping (JSON) -> Void,
                    failure : @escaping (Error) -> Void) {
        
        request(URLString: endPoint.path,
                httpMethod: .get,
                parameters: parameters,
                encoding: URLEncoding.queryString,
                headers: headers,
                loader: loader,
                success: success,
                failure: failure)
    }
    
    
    static func PUT(endPoint : WebServices.EndPoint,
                    parameters : JSONDictionary = [:],
                    headers : HTTPHeaders = [:],
                    loader : Bool = true,
                    success : @escaping (JSON) -> Void,
                    failure : @escaping (Error) -> Void) {
        
        request(URLString: endPoint.path,
                httpMethod: .put,
                parameters: parameters,
                headers: headers,
                loader: loader,
                success: success,
                failure: failure)
    }
    
    
    static func DELETE(endPoint : WebServices.EndPoint,
                       parameters : JSONDictionary = [:],
                       headers : HTTPHeaders = [:],
                       loader : Bool = false,
                       success : @escaping (JSON) -> Void,
                       failure : @escaping (Error) -> Void) {
        
        request(URLString: endPoint.path,
                httpMethod: .delete,
                parameters: parameters,
                headers: headers,
                loader: loader,
                success: success,
                failure: failure)
    }
    
    static func PATCH(endPoint : WebServices.EndPoint,
                      parameters : JSONDictionary = [:],
                      headers : HTTPHeaders = [:],
                      loader : Bool = true,
                      success : @escaping (JSON) -> Void,
                      failure : @escaping (Error) -> Void) {
        
        request(URLString: endPoint.path,
                httpMethod: .patch,
                parameters: parameters,
                headers: headers,
                loader: loader,
                success: success,
                failure: failure)
    }
    
    
    private static func request(URLString : String,
                                httpMethod : HTTPMethod,
                                parameters : JSONDictionary = [:],
                                encoding: URLEncoding = .httpBody,
                                headers : HTTPHeaders = [:],
                                loader : Bool = true,
                                success : @escaping (JSON) -> Void,
                                failure : @escaping (Error) -> Void) {
        
        if loader { showLoader() }
        
        let header = HTTPHeaders()
//        header["Authorization"] = "Basic \(basicTextCode)"
//        header["content-type"] = "application/x-www-form-urlencoded"
//        header["language"] = "english"
//        header["platform"] = "2"
//
//        if UserDetails.isUserLogin {
//            header["Accesstoken"] = AppUserDefaults.value(forKey: AppUserDefaults.Key.accessToken).stringValue
//        }
        
        print_debug("==================HEADER==================")
        print_debug(header)
        print_debug("==========================================")
        
        alamofireManager.request(URLString, method: httpMethod, parameters: parameters, encoding: encoding, headers: header).responseJSON { (response:DataResponse<Any>) in
            
            print_debug(URLString)
            print_debug(headers)
            
            if loader { hideLoader() }
            
            let decodedStr = NSString(data: (response.data! as Data), encoding: 4)
            
            print_debug("============================================")
            print_debug("Response Object")
            print_debug("============================================")
            print_debug("\(String(describing: decodedStr))")
            print_debug("============================================")
            
            switch(response.result) {
                
            case .success(let value):
                print_debug(value)
                
                success(JSON(value))
                return
                
            case .failure(let e):
                
                if (e as NSError).code == NSURLErrorNotConnectedToInternet {
                    
                    // Handle Internet Not available UI
                    if loader { hideLoader() }
                    
                    NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                }
                
                failure(e)
            }
        }
    }
    
    
    
}




