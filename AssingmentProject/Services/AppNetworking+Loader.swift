//
//  AppNetworking+Loader.swift
//  AssingmentProject
//
//  Created by Nitin Singh on 14/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

extension AppNetworking  {
    
    static func showLoader() {
        DispatchQueue.main.async {
            print_debug("\(#function) on \(Date())")
        }
    }
    
    static func hideLoader() {
        DispatchQueue.main.async {
            print_debug("\(#function) on \(Date())")
        }
    }
    
}
